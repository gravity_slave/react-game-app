import React, { Component } from 'react';
import './App.css';
import EasyABC from './EasyABC';

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="App-header">
            <h2>EasyABC </h2>
          <EasyABC/>
        </div>

      </div>
    );
  }
}

export default App;
