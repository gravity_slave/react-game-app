import React, { Component } from 'react';

import logo from './logo.svg';
import './App.css';

class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      headerText: "Non nobis domine, sed nomini tuo da gloriam.",
        contentText: 'Da pacem, Domine, in diebus nostris'
    };
  }

  render() {
    return (
      <div className="App">
          {/*
        <h1>{this.state.headerText}</h1>
        <p>{this.state.contentText}</p>
        */}

            <Header header={this.state.headerText}/>
            <Content content={this.state.contentText} />
          {/* <Clock increment={1}/> */}


      </div>
    );
  }
}



class Header extends Component {

  render() {
    return(
        <div className="App-header">
      <img src={logo} className="App-logo" alt="logo" />
      <h2>Welcome to  JERUSALEM</h2>
    </div>
    );
  }
}


class Content extends Component {
    constructor(props) {
        super(props);
        this.state = {
            myInputValue: "My Input "
        };

        this.myInputChanged = this.myInputChanged.bind(this);
    }
    myInputChanged(e) {
        let itemValue = e.target.value;
        this.setState({myInputValue: itemValue});
    }
    render() {
        return(
            <div className="App-intro">
                <h1>Forms in React!</h1>
                <p>In this lecture, we will go over the Forms in React</p>
                <MyInputComponent inputValue={this.state.myInputValue}
                                  myInputChanged={this.myInputChanged}/>
                <h4>{this.state.myInputValue}</h4>
                <EssayComponent />
                <FlavorForm />
                <hr />
                <Reservation />

            </div>
        );
    }
}

class MyInputComponent extends Component {

    render() {
        return(
            <div>
                <input value={this.props.inputValue} onChange={this.props.myInputChanged}/>

            </div>
        );
    }
}


class EssayComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: "Once upon ad time"
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleChange(e) {
        this.setState({value: e.target.value});
    }

    handleSubmit(e) {
        alert(this.state.value);
        e.preventDefault();
    }
    render() {
        return(
            <form onSubmit={this.handleSubmit}>
                <textarea value={this.state.value} onChange={this.handleChange}></textarea>
                <h4>{this.state.value}</h4>
                <button type="submit">Submit</button>
                           </form>
        );
    }
}


class FlavorForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: "watermelon"
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleChange(e) {
        this.setState({value: e.target.value});
    }
    handleSubmit(e){
        alert("The Flavor you selected is : "+this.state.value.toUpperCase());
        e.preventDefault();
    }
    render() {
        return(
            <form onSubmit={this.handleSubmit}>
                <label>
                    Pick you favourite flavor

                <select onChange={this.handleChange} >
                    <option value="reaclaim Herusalem">reclaim Jerusalem</option>
                    <option value="CAll the pope">Call the pope</option>
                    <option value="DEUS VULT">Deus Vult</option>
                    <option value="Remove Kebab">Remove Kebab</option>
                    <option value="Non Npbis Domine">Non Nobis Domine</option>

                </select>
                </label>
                <input type="submit" value='Submit' />
            </form>
        );
    }
}

class Reservation extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isGoing: true,
            numberOfGuests: 2
        };

        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(e) {
        alert(`I will be going ${this.state.isGoing},  I will bring ${this.state.numberOfGuests}`);

        e.preventDefault();
    }

    handleInputChange(e) {
        const target = e.target;
        const value = target.type === "checkbox" ? target.checked : target.value;
        const name = target.name;

        console.log(name, value);
        this.setState({
            [name]: value
        });
    }
    render() {
        return(
            <form onSubmit={this.handleSubmit}>
                <label>
                    Are you going for the party? :
                    <input
                        name="isGoing"
                        type="checkbox"
                        checked={this.state.isGoing}
                        onChange={this.handleInputChange} />
                </label>
                <br/>
                <label>
                    How many guests will you bring? :
                    <input
                        name="numberOfGuests"
                        type="number"
                        value={this.state.numberOfGuests}
                        onChange={this.handleInputChange} />
                </label>
                <input type="submit" value="Submit" />
            </form>
        );
    }
}

export default App;











